#!/usr/bin/env python3
import socket
import sys
import time
from datetime import datetime

socket_path = "/run/ipc/ipc_server.socket"


def send_loop():
    with socket.socket(socket.AF_UNIX, socket.SOCK_STREAM) as sock:
        print("Connecting to:", socket_path)
        sys.stdout.flush()
        sock.connect(socket_path)

        while True:
            now = datetime.now()
            current_time = now.strftime("%H:%M:%S")
            sock.send(f"{current_time}\n".encode())
            time.sleep(1)


if __name__ == "__main__":
    while True:
        try:
            send_loop()
        except OSError as e:
            print("Connection failed:", e)
            print("Retrying")
            sys.stdout.flush()
            time.sleep(1)
