#!/bin/bash

if [ ! -f duffy.session ]; then
echo "Retrieving an AWS instance"
set +x
duffy client \
 request-session \
 pool=metal-ec2-c5n-centos-9s-x86_64,quantity=1 > duffy.session
fi

target=${1:-main}

set -x

ip=$(jq '.session.nodes[].data.provision.public_ipaddress' duffy.session)
ip=$(echo $ip | sed -e 's|"||g')
echo "IP address: $ip"
session_id=$(jq '.session.id' duffy.session)
session_id=$(echo $session_id | sed -e 's|"||g')
echo "Session: $session_id"

ssh \
    -o " UserKnownHostsFile=/dev/null" \
    -o "StrictHostKeyChecking no" \
    -o "IdentitiesOnly=yes" \
    -i automotive_sig.ssh \
    root@$ip << EOF
cat > run.sh << EO
set -x
mkdir -p /dev/shm/docs
cd /dev/shm/docs
curl -o test_all.sh \
  "https://gitlab.com/CentOS/automotive/sig-docs/-/raw/main/demos/test_all.sh?ref_type=heads"
time bash test_all.sh $target
EO
bash run.sh
EOF

success=$?
echo $success

scp -r \
    -o " UserKnownHostsFile=/dev/null" \
    -o "StrictHostKeyChecking no" \
    -o "IdentitiesOnly=yes" \
    -i automotive_sig.ssh \
    root@$ip:/dev/shm/docs/sig-docs/demos/logs/ .
echo $?

for i in `ls -1 logs/`; do
  sed -i '1 i <html><body><pre>' logs/$i
  sed -i -e '$a</pre></body></html>' logs/$i
done

echo "Closing session: $session_id"
set +x
duffy client \
 retire-session $session_id
rm duffy.session

exit $success
