#!/usr/bin/sh

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
cd $SCRIPT_DIR

image=true
while [[ $# -gt 0 ]]; do
  case $1 in
    --noimage)
      image=false
      shift # past argument
      ;;
  esac
done

arch=$(arch)

if [ $image = true ]; then
  automotive-image-builder --verbose --container \
    build \
    --distro autosd9 \
    --target qemu \
    --mode image \
    --build-dir=_build \
    --export image \
    rpm_remote.aib.yml \
    rpm_remote.$arch.img
fi
