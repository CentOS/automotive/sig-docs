#!/usr/bin/bash

target=${1:-main}

dnf copr enable -y @centos-automotive-sig/osbuild-auto
# TODO: Move back to automotive-image-builder once a new release is out (post
dnf copr enable -y @centos-automotive-sig/automotive-image-builder-dev
dnf install -y epel-release
dnf install -y git osbuild-auto automotive-image-builder \
    rpm-build podman createrepo parallel qm sshpass qemu-kvm expect

# Install dependencies of sample-apps
dnf install -y boost-devel cmake gcc-c++ make vsomeip3-devel

git clone https://gitlab.com/CentOS/automotive/sig-docs.git
cd sig-docs/demos
git checkout $target
git --no-pager log -p -1

build(){
  echo "Building image   $1...."
  pushd "$1" > /dev/null;
  bash build.sh > "$1_build".log.html 2>&1
  suc=$?
  popd  > /dev/null;
  printf "Building image   %-35s done - $suc\n" "$1...."
}

testimg(){
  echo "Testing  image   $1...."
  pushd "$1" > /dev/null;
  success="skipped"
  if [ -f "test.sh" ]; then
    bash test.sh $2 > "$1_test".log.html 2>&1
    success=$?
  fi
  echo $success > "$1_test".outcode
  popd  > /dev/null;
  printf "Testing  image   %-35s %s\n" "$1...." $success
}

# Build the local RPM before we build all the images
pushd rpm_local > /dev/null
echo -n "Building local RPM"
bash build.sh --noimage > rpm_local_1_build.log.html 2>&1
echo "...    done"
popd > /dev/null

for i in `ls -1`; do
  if [[ -d "$i" && $i != '_build' ]]; then
    build "$i" &
  fi
done

wait

echo "All builds completed"

port=2222
for i in `ls -1`; do
  if [[ -d "$i" && $i != '_build' ]]; then
    testimg "$i" $port &
    port=$((port+1))
  fi
done

wait
echo "All tests completed"


# we expect 8 outcode files
find -name "*_test.outcode" | wc -l | grep -q 8
# Check these outcode
o=$(find -name "*_test.outcode" | xargs cat | sort | uniq)
# Remove all wrong characters
o="${o//[$'\t\r\n ']}"

# Gather all the files
mkdir -p logs
find . -type f -name "*_test.log.html" -exec cp -- "{}" ./logs/ \;
find . -type f -name "*_build.log.html" -exec cp -- "{}" ./logs/ \;

pwd
ls -l
ls -l networking
ls -l logs

if [ $o -eq 0 ]; then
  echo "success"
  exit 0
else
  echo "failure"
  exit 1
fi
