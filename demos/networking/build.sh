#!/usr/bin/bash

image=true
while [[ $# -gt 0 ]]; do
  case $1 in
    --noimage)
      image=false
      shift # past argument
      ;;
  esac
done

set -xe
arch=$(arch)

if [ $image = true ]; then
   automotive-image-builder --verbose --container \
    --include=.. \
    build \
    --distro autosd9 \
    --target qemu \
    --mode image \
    --build-dir=_build \
    --export image \
    networking.aib.yml \
    networking.$arch.img
fi
