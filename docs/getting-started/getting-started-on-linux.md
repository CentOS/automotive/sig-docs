# Getting started on Linux

Set up your development environment with the prerequisite tools and repositories you need to quickly start building
AutoSD images.

**Prerequisites**

* A physical or virtual Linux host machine
* The `automotive-image-builder` tool. For more information, see
[Installing the `automotive-image-builder` tool](../getting-started/about-automotive-image-builder.md)
* git

## Quick start: Building AutoSD images

To build an AutoSD image, you need the `automotive-image-builder` tool and a compatible automotive image builder manifest.
You can find several sample manifests in the **images** directory in the [sample-images](https://gitlab.com/CentOS/automotive/sample-images)
repository.

**Procedure**

1. Copy or modify one of the sample manifests from the
[sample-images/images](https://gitlab.com/CentOS/automotive/sample-images/-/tree/main/images?ref_type=heads) directory.

    ```console
    $ git clone https://gitlab.com/CentOS/automotive/sample-images.git
    ```

1. Build an image with `automotive-image-builder`:

    ```console
    $ sudo automotive-image-builder build \
    --target qemu \
    --export qcow2 \
    sample-images/images/simple-developer.aib.yml \
    my-image.qcow2
    ```

    In this example, include the `simple-developer.aib.yml` automotive image builder manifest file to build a developer image.
    The developer image has a number of utilities installed for development purposes.
    For more information about the Automotive image builder manifests, see [Introducing Automotive image builder](about-automotive-image-builder.md).
    For more information about creating a custom manifest, see [Deploying software on AutoSD](../building/deploying_sw_in_the_os_image.md).

    The `build` command takes a number of inputs:

    * Use `--target` to set the target environment. The default is `qemu`.
    Use `qemu` to build an image that you can launch in a QEMU virtual machine. Run `list-targets` to view the list of available options.
    * Use `--distro` to define the package repository that you want to use for the image build.
    The default is CentOS Stream 9 (`cs9`). Run `list-dist` to view the list of available options.
    You can also extend this list with your own custom distribution.
    For more information, see [distributions](about-automotive-image-builder.md#distributions).
    * Use `--export` to set the export file format. Run `list-exports` to view the list of available options.
    * Use `--mode` to set the type of OS image. This can be `package`, to build a package-based operating system image, or `image` to build an
    OSTree image. The default is `image`.

## Quick start: Booting prebuilt AutoSD images in a QEMU VM

A virtualized AutoSD development environment is useful for building and testing applications intended to run on an AutoSD system,
or to build new AutoSD images that you can flash onto automotive hardware or use on other systems.

To obtain an AutoSD image that you can boot in a VM, you can either build your own following
[Quick-start: building AutoSD images](#quick-start-building-autosd-images), or download one of the
[nightly developer images](https://autosd.sig.centos.org/AutoSD-9/nightly/sample-images/), and then launch a VM from the image.
You can then use this VM as your development environment, where you can customize and build your own AutoSD images.

**Prerequisites**

* A physical or virtual Linux host machine
* The `automotive-image-builder` tool. For more information, see
  [Installing Automotive imag builde](index.md#installing-automotive-image-builder)
* [QEMU](https://www.qemu.org/download/#linux)
* A developer AutoSD image. Either your own image that you built from the `simple-developer.aib.yml` manifest,
  or the developer nightly image.

**Procedure**

<!---
1. Identify the name of the latest nightly image for your host architecture, and store the value in a variable called `AUTOSD_IMAGE_NAME`.

    !!! note

        The Automotive SIG uploads images every day using unique build IDs, which causes the name of the image to change frequently.
        These commands use the RPM-based developer AutoSD sample image for x86_64 as an example, but you can modify the commands
        to retrieve the latest nightly for any prebuilt sample image.
        For more information about available nightly images, their purposes, and their naming conventions,
        see [Sample OS images](sample-non-sample-images.md).

      1. If your host machine has an `x86_64` CPU, retrieve the name of the latest `x86_64` image:

        ```console
        $ export AUTOSD_IMAGE_NAME="$(curl https://autosd.sig.centos.org/AutoSD-9/nightly/sample-images/ | \
        grep -oE 'auto-osbuild-qemu-autosd9-developer-regular-x86_64-([0-9]+)\.([A-Za-z0-9]+)\.qcow2\.xz' | \
        head -n 1)"
        ```

      1. If your host machine has an `aarch64` SoC, retrieve the name of the latest `aarch64` image:

        ```console
        $ export AUTOSD_IMAGE_NAME="$(curl https://autosd.sig.centos.org/AutoSD-9/nightly/sample-images/ | \
        grep -oE 'auto-osbuild-qemu-autosd9-developer-regular-aarch64-([0-9]+)\.([A-Za-z0-9]+)\.qcow2\.xz' | \
        head -n 1)"
        ```

1. Download the image:

    ```console
    $ curl -o autosd9-dev-reg-x86_64.qcow2.xz https://autosd.sig.centos.org/AutoSD-9/nightly/sample-images/$AUTOSD_IMAGE_NAME
    ```

1. Uncompress the compressed `.xz` image file:

    ```console
    $ xz -d autosd9-dev-reg-x86_64.qcow2.xz
    ```
-->

1. Launch a VM from the image with the `automotive-image-runner` tool. This tool comes packaged with `automotive-image-builder`.

    ```console
    $ sudo automotive-image-runner <path>/<to>/<image>/image.qcow2
    ```

1. Log in as the `root` user with the default password, `password`.

    !!! NOTE
        To enable ssh access, you must set `PasswordAuthentication yes` in `/etc/ssh/sshd_config`. Then you can access the machine
        with `ssh -p 2222 -o "UserKnownHostsFile=/dev/null" guest@localhost`.

## Quick start: Building customized AutoSD images in a QEMU VM

Repeat the procedure in
[Quick start: Booting prebuilt AutoSD images in a QEMU VM](#quick-start-booting-prebuilt-autosd-images-in-a-qemu-vm),
to download and run the latest nightly developer image. Then, expand the disk size, so you can use the
`automotive-image-builder` tool to create customized system images using your custom `.aib.yml` manifest file.

For more information about the preconfigured manifest files the Automotive SIG provides as starter examples you can modify, see the
[Automotive Image Builder example manifests](about-automotive-image-builder.md#automotive-image-builder-example-manifests).

For more in-depth information about how to package your applications and embed them in a customized manifest you can then use to generate
your customized OS image, see
[Packaging applications with RPM](../building/packaging_apps_with_rpm.md) and
[Embedding RPM packages in the AutoSD image](../building/packaging_apps_with_rpm.md#embedding-rpm-packages-from-local-storage-into-the-autosd-image) sections.

**Prerequisites**

* A physical or virtual Linux host machine
* The `automotive-image-builder` tool. For more information, see
[Installing Automotive image builder](index.md#installing-automotive-image-builder)
* [QEMU](https://www.qemu.org/download/#linux)
* A developer AutoSD image. Either your own image that you built from the `simple-developer.aib.yml` manifest, or the developer
nightly image.

**Procedure**

1. Extend the virtual disk of your `.qcow2` developer image, so that you have enough space to build
your custom AutoSD images and facilitate your development work.

      1. On the host, resize your developer image. In this example, set the disk size to `30G`, which is 30GiB:

         ```console
         $ qemu-img resize <image>.qcow2 30G
         ```

1. Launch your virtual AutoSD developer environment:

    ```console
    $ sudo automotive-image-runner <path>/<image>.qcow2
    ```

1. Log in with the `guest` user and the default password, `password`.

    !!! NOTE
        To enable ssh access, you must set `PasswordAuthentication yes` in `/etc/ssh/sshd_config`. Then you can access the machine
        with `ssh -p 2222 -o "UserKnownHostsFile=/dev/null" guest@localhost`.

1. Change to the `root` user. The `root` user password is also `password`:

    ```console
    $ su -
    ```

1. Install the `parted` partition management tool to extend the file system:

    ```console
    # dnf -y install parted
    ```

    1. Run `parted` to extend the size of `/dev/vda`:

        ```console
        # parted /dev/vda
        ```

    1. Resize the `/dev/vda3` partition to fill the space available to that partition:

        ```console
        (parted) resizepart 3 100%
        ```

    1. Exit the `parted` tool:

        ```console
        (parted) quit
        ```

    1. Enlarge the file system:

        ```console
        # resize2fs /dev/vda3
        ```

1. Install `automotive-image-builder` in your development VM.

    1. Enable the `automotive-image-builder` repository:

        ```console
        # dnf copr enable @centos-automotive-sig/automotive-image-builder
        ```

    1. Install the `automotive-image-builder` tool:

        ```console
        # dnf install automotive-image-builder
        ```

1. In your development VM, create a custom Automotive image builder manifest file that you can configure according to your requirements:

      ```console
      # vim my-manifest.aib.yml
      ```

    !!! note
        The Automotive SIG provides several sample manifest files in `sample-images/images/` you can reference. For more information about
        available sample images, see [Sample OS images](sample-non-sample-images.md). To view an example customized manifest, see
         [Sample automotive image builder manifest](../building/ref_sample-yaml.md). For more details about how to build and customize images, see
         [Deploying software on AutoSD](../building/deploying_sw_in_the_os_image.md) and
         [Embedding RPM packages in the AutoSD image](../building/packaging_apps_with_rpm.md#embedding-rpm-packages-from-local-storage-into-the-autosd-image).

1. Build the OS image from your custom `my-manifest.aib.yml`. In this example, build a `qcow2` format for the `qemu` target so that you can
launch your image in a VM:

    ```console
    # automotive-image-builder build --target qemu --export qcow2 my-manifest.aib.yml my-image.qcow2
    ```

    !!! NOTE
        For more information about the export file types `automotive-image-builder` supports,
        see [Export formats](about-automotive-image-builder.md#export-formats) in the
        [`automotive-image-builder` options](about-automotive-image-builder.md#understanding-the-automotive-image-builder-options) section.

        For more information about image naming conventions, see [AutoSD sample images](sample-non-sample-images.md#autosd-sample-images).

1. Exit the VM and export the image file to the host:

    ```console
    $ scp -P 2222 "UserKnownHostsFile=/dev/null" guest@localhost:/home/guest/my-image.qcow2 .
    ```

1. On the host, launch a VM from your new image:

    ```console
    $ sudo automotive-image-runner my-image.qcow2
    ```

For more information about how to build your own customized AutoSD images, see [Building and running your customized OS image](../building/building_an_os_image.md#building-and-running-your-customized-os-image).
