# Getting started on macOS

Set up your development environment with the prerequisite tools and repositories that you need to quickly start building
AutoSD images in either a preconfigured container or a virtual machine (VM).

**Prerequisites**

* A machine running macOS.
* [Homebrew](https://brew.sh/)
* A local clone of the [AutoSD sample-images repository](https://gitlab.com/CentOS/automotive/sample-images)

## Quick start: Building AutoSD images

You can use the preconfigured AutoSD development container as a lightweight base development environment if you want to
build an AutoSD image without launching and configuring a development VM.

**Prerequisites**

* [Podman](https://podman.io/docs/installation#macos)
* A local clone of the [AutoSD sample-images repository](https://gitlab.com/CentOS/automotive/sample-images)

**Procedure**

1. Install [Podman 5](https://podman.io/docs/installation#macos):

    ```console
    $ brew install podman
    ```

1. Initialize a Podman machine with rootful support for OSBuild:

    ```console
    $ podman machine init --rootful
    ```

1. Start the Podman machine:

    ```console
    $ podman machine start
    ```

1. Navigate to the `sample-images` directory:

    ```console
    $ cd sample-images
    ```

1. Make the `auto-image-builder.sh` script executable:

    ```console
    $ chmod +x auto-image-builder.sh
    ```

1. Build an image with the `auto-image-builder.sh`:

    ```console
    $ sudo ./auto-image-builder.sh build --target qemu --export qcow2 images/simple-developer.aib.yml my-image.qcow2
    ```

    In this example, include the `images/simple-developer.aib.yml` OSBuild manifest file to build a developer image. The developer image
    has a number of utilities installed for development purposes.
    For more information about the Automotive image builder manifests, see [Introducing Automotive image builder](about-automotive-image-builder.md).
    For more information about creating a custom manifest, see [Deploying software on AutoSD](../building/deploying_sw_in_the_os_image.md).

    The `build` command takes a variety of inputs:

    * Use `--target` to set the target environment. The default is `qemu`.
    Use `qemu` to build an image that you can launch in a QEMU virtual machine. Run `--list-targets` to view the list of available options.
    * Use `--distro` to define the package repository that you want to use for the image build.
    The default is CentOS Stream 9 (`cs9`). Run `--list-dist` to view the list of available options.
    You can also extend this list with your own custom distribution.
    For more information, see [distributions](about-automotive-image-builder.md#distributions).
    * Use `--export` to set the export file format. Run `--list-exports` to view the list of available options.
    * Use `--mode` to set the type of OS image. This can be `package`, to build a package-based operating system image, or `image` to build an
    OSTree image. For development and testing purposes, use `package`. For production, use `image`.

## Quick start: Booting prebuilt AutoSD images in a QEMU VM

A virtualized AutoSD development environment is similar to a AutoSD production environment. The virtualized platform is useful for building and
testing applications intended to run on a AutoSD system or to build new AutoSD images that you can flash onto automotive hardware or use on other
systems.

Download and uncompress a prebuilt AutoSD operating system (OS) image, and then launch a VM from the image. This VM is your
development environment, where you can customize and build your own AutoSD images.

All prebuilt AutoSD images can be found in the [nightly repo](https://autosd.sig.centos.org/AutoSD-9/nightly/).
The repo features both x86_64 and aarch64 CPU architecture images. To explore the sample images, see
[the nightly sample images](https://autosd.sig.centos.org/AutoSD-9/nightly/sample-images/).

This following example build uses the nightly aarch64 `developer` image. For this example to work, you must be running macOS on a machine
with an Apple Silicon ARM processor. If you are running macOS on a machine with x86_64 processor, you can adapt the procedure by downloading
the developer image built for the X86_64 architecture instead.

**Prerequisites**

* A local clone of the [AutoSD sample-images repository](https://gitlab.com/CentOS/automotive/sample-images)

**Procedure**

1. Install [QEMU](https://www.qemu.org/download/#macos) and
  [XZ Utils](https://github.com/tukaani-project/xz):

    ```console
    $ brew install qemu xz
    ```

1. Go to the `sample-images` directory where you cloned the GitLab `sample-images` repository.

1. Identify the name of the latest nightly image for your host architecture, and store the value in a variable called `AUTOSD_IMAGE_NAME`:

      ```console
      export AUTOSD_IMAGE_NAME="$(curl https://autosd.sig.centos.org/AutoSD-9/nightly/sample-images/ | \
      grep -oE 'auto-osbuild-qemu-autosd9-developer-regular-aarch64-([0-9]+)\.([A-Za-z0-9]+)\.qcow2\.xz' | \
      head -n 1)"
      ```

    !!! note

        The Automotive SIG uploads images every day using unique build IDs, which causes the name of the image to change frequently.
        For more information about available nightly images, their purposes, and their naming conventions,
        see [Sample OS images](sample-non-sample-images.md).

1. Download the image:

    ```console
    $ curl -o autosd9-developer-regular-aarch64.qcow2.xz https://autosd.sig.centos.org/AutoSD-9/nightly/sample-images/$AUTOSD_IMAGE_NAME
    ```

1. Uncompress the compressed `.xz` image file:

    ```console
    $ xz -d autosd9-developer-regular-aarch64.qcow2.xz
    ```

1. From the `sample-images` directory, launch a VM from the image with the
  [`automotive-image-runner`](https://gitlab.com/CentOS/automotive/src/automotive-image-builder/-/blob/main/automotive-image-runner?ref_type=heads)
  script in the `automotive-image-builder` directory of your local clone of the
[AutoSD sample-images repository](https://gitlab.com/CentOS/automotive/sample-images):

    ```console
    $ automotive-image-builder/automotive-image-runner --nographics <path>/autosd9-developer-regular-aarch64.qcow2
    ```

1. Log in as the `root` user with the default password, `password`.

    !!! NOTE
        To enable ssh access to the guest account, you must set `PasswordAuthentication yes` in `/etc/ssh/sshd_config` within the virtual machine.
        Then you can access the virtual machine from a terminal window on your local machine with `ssh -p 2222 -o "UserKnownHostsFile=/dev/null" guest@localhost`.

## Quick start: Building customized AutoSD images in a QEMU VM

Repeat the procedure in
[Quick start: Booting prebuilt AutoSD images in a QEMU VM](#quick-start-booting-prebuilt-autosd-images-in-a-qemu-vm),
to download and run the latest nightly developer image, which has extra storage compared to the other sample images. Then, expand
the disk size, so you can use Automotive image builder to create customized AutoSD system images using your custom manifest `.aib.yml` file.

For more information about the preconfigured manifest files the Automotive SIG provides as starter examples you can modify, see the
[Automotive image builder example manifests](about-automotive-image-builder.md#automotive-image-builder-example-manifests).

For more in-depth information about how to package your applications and embed them in a customized manifest that you can then use to generate
your customized OS image, see
[Packaging applications with RPM](../building/packaging_apps_with_rpm.md) and
[Embedding RPM packages in the AutoSD image](../building/packaging_apps_with_rpm.md#embedding-rpm-packages-from-local-storage-into-the-autosd-image) sections.

**Prerequisites**

* The latest [nightly](https://autosd.sig.centos.org/AutoSD-9/nightly/sample-images/)
  `auto-osbuild-qemu-autosd9-minimal-ostree-aarch64-<version>.qcow2` image. If you have not done so already, follow the procedure in
    [Quick start: Booting prebuilt AutoSD images in a QEMU VM](#quick-start-booting-prebuilt-autosd-images-in-a-qemu-vm),
   to download the latest nightly developer image.

**Procedure**

1. Extend the virtual disk of your `.qcow2` development image to ensure that
your AutoSD image has enough space to facilitate your custom development work.

      1. On the macOS host, resize your development image. In this example, set the disk size to `100G`, which will enable you to resize
        the main disk partition to 100 gigabytes in the VM:

         ```console
         $ qemu-img resize autosd9-developer-regular-aarch64.qcow2 100G
         ```

      1. Launch your virtual AutoSD development environment from the `sample-images` directory:

         ```console
         $ automotive-image-builder/automotive-image-runner --nographics autosd9-developer-regular-aarch64.qcow2
         ```

      1. Log in with the `guest` user and the default password `password`.

        !!! NOTE
            To enable ssh access, you must set `PasswordAuthentication yes` in `/etc/ssh/sshd_config`. Then, you can access the booted virtual machine from your macOS local host by using the following command:
             `ssh -p 2222 -o "UserKnownHostsFile=/dev/null" guest@localhost`.

1. Change to the `root` user. The `root` user password is `password`:

    ```console
    $ su -
    ```

1. Install the `parted` partition management tool and the `e2fsprogs` file system utilities so that you can expand the file system:

    ```console
    # dnf -y install parted e2fsprogs
    ```

    1. Run `parted` to extend the size of `/dev/vda`:

        ```console
        # parted /dev/vda
        ```

    1. Resize the `/dev/vda3` partition to fill the space available to that partition:

        ```console
        (parted) resizepart 3 100%
        ```

    1. If prompted to fix the GPT to use all available space, enter `fix`.

    1. If prompted for a partition number, enter `3`, and when prompted to continue, enter `yes`.

    1. At the `End?` prompt, enter `102400`, which will extend the partition size to 100GB.

    1. Exit the `parted` tool:

        ```console
        (parted) quit
        ```

    1. Resize the file system:

        ```console
        # resize2fs /dev/vda3
        ```

    1. Confirm that the `/dev/vda3` partition has been resized to around 100GB:

        ```console
        # df -h /dev/vda3
        ```

1. Install Automotive image builder by following these steps:

     1. Install dnf plugin tools so that you can enable a copr repository:

          ```console
          # dnf install 'dnf-command(copr)'
          ```

     1. Enable the `centos-automotive-sig/automotive-image-builder` copr repository:

          ```console
          # dnf copr enable @centos-automotive-sig/automotive-image-builder
          ```

     1. Install Automotive image builder:

          ```console
          # dnf install automotive-image-builder
          ```

1. Clone the AutoSD `sample-images` GitLab repository in your devlopment VM:

     ```console
     # git clone --recursive https://gitlab.com/CentOS/automotive/sample-images.git
     ```

1. In your development VM in the `sample-images` directory you just cloned, create a custom Automotive image builder manifest file that you can
    configure according to your requirements:

      ```console
      # vim my-manifest.aib.yml
      ```

    !!! note
        The Automotive SIG provides several sample `.aib.yml` manifest files in `sample-images/images/` that you can reference or copy. For more
        information about available sample images, see [Sample OS images](sample-non-sample-images.md). To view an example customized manifest, see
         [Sample customized Automotive image builder manifest](../building/ref_sample-yaml.md). For more details about how to build and
         customize images, see [Deploying software on AutoSD](../building/deploying_sw_in_the_os_image.md) and
         [Embedding RPM packages in the AutoSD image](../building/packaging_apps_with_rpm.md#embedding-rpm-packages-from-local-storage-into-the-autosd-image).

1. If you have not done so already, add the following code to the `auth:` section of your custom `my-manifest.aib.yml` file to allow ssh
    password authentication and root access to the running VM:

    ```console
    --8<-- "demos/networking/networking.aib.yml:30:31"
    ```

1. Build the OS image from your custom `my-manifest.aib.yml`. In this example, build a `qcow2` format for the `qemu` target so that you can
    launch your image in a VM:

    ```console
    # automotive-image-builder build --target qemu --export qcow2 my-manifest.aib.yml my-image.qcow2
    ```

    !!! NOTE
        For more information about the export file types that Automotive image builder supports, see [Export formats](about-automotive-image-builder.md#export-formats)
         in the [Automotive image builder options](about-automotive-image-builder.md#understanding-the-automotive-image-builder-options) section.

        For more information about image naming conventions, see [AutoSD sample images](sample-non-sample-images.md#autosd-sample-images).

1. With the VM still running, open a new terminal window on your macOS host machine, change to the `sample-images` repository directory, and
    import your custom `my-image.qcow2` OS image file from the VM to the host:

    ```console
    $ scp -P 2222 "UserKnownHostsFile=/dev/null" root@localhost:/root/sample-images/my-image.qcow2 .
    ```

1. On the macOS host, while still in the `sample-images` directory, launch a VM from your new image:

    ```console
    $ automotive-image-builder/automotive-image-runner --nographics my-image.qcow2
    ```

For more information about how to build your own customized AutoSD images, see [Building and running your customized OS image](../building/building_an_os_image.md#building-and-running-your-customized-os-image).
