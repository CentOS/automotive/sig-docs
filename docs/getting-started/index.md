# Getting started

Learn about the build tool, the sample build manifests, and the sample images, and then follow one of the 'Getting started' workflows.

**automotive-image-builder**
: The build tool that takes YAML-formatted manifests as input, and produces operating system images according to your
requirements. For more information, see [about automotive-image-builder](about-automotive-image-builder.md).

**Sample manifests**
: You can view a collection of sample manifests in the [sample-images](https://gitlab.com/CentOS/automotive/sample-images)
repository. These sample manifests are useful to understand the schema and syntax, and can be a useful basis for you to create
your own custom manifests. For more information, see [Automotive image builder example manifests](about-automotive-image-builder.md#automotive-image-builder-example-manifests).

**Sample images**
: The Automotive SIG builds a number of images nightly, which you can download and explore. You can also use them as virtual
development environments for building your own images on systems other than RHEL, CentOS, or Fedora. For more information, see
[Sample OS images](sample-non-sample-images.md).

## Installing Automotive image builder

Automotive image builder is packaged as an RPM, available for RHEL, CentOS, and Fedora from the
[centos-automotive-sig](https://copr.fedorainfracloud.org/coprs/g/centos-automotive-sig/automotive-image-builder/)
copr repository.

**Procedure**

1. Enable the `automotive-image-builder` repository:

    ```console
    $ sudo dnf copr enable @centos-automotive-sig/automotive-image-builder
    ```

1. Install the `automotive-image-builder` tool. This RPM also includes `automotive-image-runner`, which you can use
to launch your AutoSD `.qcow2` images with QEMU, as well as other tools that the build process relies on.

    ```console
    $ sudo dnf install automotive-image-builder
    ```

For more information about installing Automotive image builder on MacOS, see [Getting started on MacOS](getting-started-on-macos.md)

## Sample manifests

You can find a variety of sample image manifests in the [sample-images](https://gitlab.com/CentOS/automotive/sample-images)
repository.

**Procedure**

1. Clone the [AutoSD sample images repository](https://gitlab.com/CentOS/automotive/sample-images):

    ```console
    $ git clone https://gitlab.com/CentOS/automotive/sample-images.git
    ```

**Next steps**

Set up a development environment on your [Linux](getting-started-on-linux.md) or
[MacOS](getting-started-on-macos.md) <!--, or [Windows](getting-started-on-windows.md)-->host and start building images.
