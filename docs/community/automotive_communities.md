
# The CentOS Automotive SIG and other communities

The CentOS Automotive SIG and its resulting AutoSD images propose solutions
that answer the automotive industry's strict demands.
The [Automotive SIG sample manifests](https://gitlab.com/centos/automotive/sample-images)
repository contains manifests that you can use to produce
OS images based on AutoSD but with customized boots, CPUs, or image types.
The Automotive SIG also collaborates with
many automotive free and open source (FOSS) upstream foundations and communities and work done with them
can land in this same repository.

## SOAFEE

The Scalable Open Architecture for Embedded Edge (SOAFEE) project is an
industry-led collaboration defined by automakers, semiconductor suppliers,
open source and independent software vendors, and cloud technology leaders.

The SOAFEE initiative delivers a cloud-native architecture enhanced for
mixed-criticality automotive applications with corresponding open-source
reference implementations to enable commercial and non-commercial offerings,
refer [soafee.io](https://www.soafee.io/).
