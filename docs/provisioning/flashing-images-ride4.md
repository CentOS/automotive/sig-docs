# Flashing images on Qualcomm Snapdragon Ride SX 4.0

## Obtaining images

To flash AutoSD onto the Ride 4 board, you need two images:

`aboot.img`
:  This is an Android boot image. It contains the automotive kernel, the initial RAM file system (initramfs), Device Tree Blob (DTB), and the kernel
command line. The boot loader reads this image to start the kernel. You flash `aboot.img` to the `boot_a` partition, which is appointed for the
Android boot image.

`rootfs.simg`
:  This is a sparse disk image. A sparse disk image grows in size, as required, to accommodate data without storing empty space. `rootfs.simg`
contains the root file system partition. The file system organizes files installed from RPM packages and files generated for configuration purposes.

You can obtain these images by using the following methods:

* Download the nightly images. The AutoSD CI pipeline builds these images each night.
* Build the images. Use tools provided by AutoSD to build example images or customize your own images.

### Downloading nightly images

Download and uncompress prebuilt nightly AutoSD `aboot.img` and `rootfs.simg` images for Ride 4.

1. Identify the name of the latest nightly image and store the value in a variable called `latest`:

      ```console
      $ latest=$(curl -s 'https://autosd.sig.centos.org/AutoSD-9/nightly/RideSX4/?C=M;O=D' | sed -ne \
      's|^<img.*href="(auto-osbuild-ridesx4-autosd9-developer-regular[^@"]\+/\)".*|\1|p' | head -n1)
      ```

1. Download the images from the Automotive SIG nightly builds repository:

      ```console
      $ wget -np -R "index.html*" -nH --cut-dirs=3 --recursive \
      "https://autosd.sig.centos.org/AutoSD-9/nightly/RideSX4/$latest"
      ```

      This command downloads a directory that contains the compressed image files.

1. Uncompress the `aboot.img.xz` and `rootfs.simg.xz` files:

      ```console
      $ xz -d "$latest/aboot.img.xz" "$latest/rootfs.simg.xz"
      ```

**Next steps**

Now that you have downloaded and uncompressed the image files, you can flash the images onto the Ride 4 board.

### Building images

To build AutoSD images, use Automotive image builder, which is available in the
[automotive-image-builder](https://gitlab.com/CentOS/automotive/src/automotive-image-builder) repository. This repository contains a configuration
file for Qualcomm Ride 4 that you can use to build images for your Ride 4 board.

**Prerequisites**

* An AArch64 system with internet access

1. Install Automotive image builder for your OS. For more information, see
[Introducing Automotive image builder](../getting-started/about-automotive-image-builder.md).

1. Clone the `sample-images` repository:

      ```console
      $ git clone https://gitlab.com/CentOS/automotive/sample-images.git
      ```

1. Change to the sample-images repository:

      ```console
      cd sample-images
      ```

1. Build the images:

      ```console
      $ automotive-image-builder --container build --distro autosd9 \
        --mode package --target ridesx4 --export aboot.simg \
        images/developer.mpp.yml autosd-ridesx4-developer-regular
      ```

      This command creates the `autosd-ridesx4-developer-regular` directory, which contains the `aboot.img` and `rootfs.simg` image files.

**Next steps**

Now that you have built the image files, you can flash the images onto the Ride 4 board.

## Flashing images

Flash the AutoSD images onto the Ride 4 board.

For more information about how to install and run ES12 on Ride 4, see the
[Release Notes](https://docviewer.qualcomm.com/releasenote/RNO-240125184101) on the Qualcomm document viewer site.

!!! note
      Qualcomm credentials are required to access the Qualcomm document viewer site. For more information, see
      [Create a Qualcomm ID](https://myaccount.qualcomm.com/signup).

**Prerequisites**

* Qualcomm Ride 4 SA8775p development system, running release version ES12 r00021.1a
* A USB cable
* `android-tools`
* Images that you obtained either through [Downloading nightly images](#downloading-nightly-images) or [Building images](#building-images)

1. Connect a USB cable from your workstation to the `USB0` port on the Ride 4.
2. From the workstation, identify the System on Chip (SoC) device:

      ```console
      $ R4SX_TTY="/dev/$(basename $(readlink /dev/serial/by-id/usb-FTDI_Qualcomm_AIR_8775_*-if01-port0))"
      ```

3. From the workstation, use a TTY application to connect to the SoC device and log in as the `root` user.

    !!! note
        The workstation in this step must be connected to the SoC device through USB. The workstation must be configured to use `fastboot`.

4. Reboot the SoC device and quickly press and hold the down arrow key until you see boot messages on the terminal window.
5. From the workstation, verify that the SoC device is in a fastboot state:

      ```console
      # fastboot devices
      c2b86509    fastboot
      ```

    !!!note
        The command returns an ID that confirms device is ready to flash. In this  example, the ID is `c2b86509`.

6. Flash the `boot_a` and `system_a` partitions with the images:

      ```console
      # fastboot flash boot_a aboot.img
      # fastboot flash system_a rootfs.simg
      ```

7. Continue to boot the SoC device:

      ```console
      # fastboot continue
      ```

8. Log in to the operating system (OS) using user ID `root` and password `password`.

**Additional Resources:**

* [Automotive SIG sample images](https://gitlab.com/CentOS/automotive/sample-images)
* [Building images](https://sigs.centos.org/automotive/building/)
