# Building and flashing images on Renesas R-Car S4

To flash AutoSD onto your R-Car S4 board, download the Renesas boot firmware and AutoSD board support package, host the AutoSD image
on a TFTP server, and configure your board to communicate with the server.

## Installing the boot firmware

To install the boot firmware for the R-Car S4, download and install the Renesas boot firmware files and the CentOS Stream Automotive
Special Interest Group (SIG) board support package from COPR.

### Getting Renesas firmware

To install the boot firmware for the R-Car S4, download the startup guide, Tera Term macros, and boot firmware files from the
 [R-Car S4 support page](https://www.renesas.com/us/en/products/automotive-products/automotive-system-chips-socs/r-car-s4-automotive-system-chip-soc-car-servercommunication-gateway).
 These files are in the Software Downloads section and require login credentials from Renesas.

**Prerequisites**

* Access to the Renesas [R-Car S4 support page](https://www.renesas.com/us/en/products/automotive-products/automotive-system-chips-socs/r-car-s4-automotive-system-chip-soc-car-servercommunication-gateway)
* A workstation PC configured with a TFTP server
* A network connection between the PC and the R-Car S4 board

**Procedure**

1. Download the Startup Guide zip file, R-Car S4 SDK Start Up Guide PKG v3.16.8.

    1. Extract the `R-CarS4_SDK_StartupGuide_3.16.8.zip` file from the archive. This archive has the `R-CarS4_SDK_StartupGuide.pdf` guide
    and the `teratermmacro` folder.

1. Download the firmware files from R-Car S4 SDK Linux PKG with GCC compiler v3.16.8.

    1. Download `xOS3_SDK1_Linux_ReferenceSDK1_v3.16.8_release.zip`.

    1. Unpack the archive, then unpack the included `rcar-xos_sdk1-gateway_v3.16.8_release.tar.gz` file. The boot firmware is located
    in the `rcar-xos/v3.16.8/os/bootloader/s4` directory.

1. To install the firmware, follow the instructions in section 18 of the `R-CarS4_SDK_StartupGuide.pdf`, "How to flash images."

### Installing the automotive board support package

Install u-boot with abootimg support from the
[board support package](https://copr.fedorainfracloud.org/coprs/g/centos-automotive-sig/renesas-board-support/package/renesas-uboot-tools/).

**Procedure**

1. Download the binary RPM file from the latest COPR build (for example, `renesas-uboot-images-armv8-2010.10-5.1.1.rc9.2.4.el9.aarch64.rpm`).

1. Extract the `u-boot.bin` file from the RPM file by using the following command or by using a similar tool:

    ```console
    rpm2cpio filename | cpio -id
    ```

1. Place the `u-boot.bin` file on the TFTP server.

1. Enter the U-Boot command line and set up networking per the instructions in
   [Configuring networking in U-Boot](flashing-renesas.md#configuring-networking-in-u-boot).

1. Load the `u-boot.bin` file, and write it to flash memory by using the following commands:

    ```console
    mmc dev 0 1
    mw.b 0x48000000 0 40000
    tftp 0x48000000 u-boot.bin
    mmc write 0x48000000 7C00 0x502
    ```

    !!! Important
        Confirm that the file and the size is correct. The `0x502` corresponds to the
        512 byte blocks of the `u-boot.bin` file size.

1. Reboot the board by using the `reset` command.

1. Verify that the U-Boot `version` command returns the version matching the RPM release (for example, `U-Boot 2020.10-5.1.1.rc9.2.4.el9`).

## Installing the AutoSD Image

To flash AutoSD onto your R-Car S4 board, you must host the AutoSD image on a TFTP server and configure the board to
communicate with the server.

**Prerequisites**

* Installed and functional boot firmware
* A workstation PC configured with a TFTP server
* A network connection between the PC and the R-Car S4 board

### Configuring networking in U-Boot

Configure networking on your R-Car S4 so that the board can communicate with the TFTP server that hosts your AutoSD image.

**Procedure**

1. Connect port 1 of the 3 ports on the Ethernet subboard.

!!! note
    Identify port 1 from the printed circuit board (PCB) label (silkscreen) next to the connector.
    Of the three connectors labeled CN101, CN201, and CN301, port 1 is CN101.
    When you examine the RJ45 connector side of the board, port 1 is located further from the other 2 ports and on the right.

2. Set the networking environment variables:

    ```console
    $ setenv ethaddr __<xx:xx:xx:xx:xx:xx>__
    $ setenv ipaddr 192.168.0.20
    $ setenv netmask 255.255.255.0
    $ setenv serverip 192.168.0.1
    $ saveenv
    ```

    Insert values that match the values for your network setup:

    * `ethaddr` specifies the MAC address from the sticker attached to the Ethernet connector
    * `ipaddr` specifies the IP address of the S4 board
    * `netmask` specifies the standard netmask
    * `serverip` specifies the IP address of the PC hosting the TFTP server

### Building the image

To build AutoSD images, use the automotive-image-builder tool. See
[Installing the automotive-image-builder tool](../getting-started/about-automotive-image-builder.md/#installing-automotive-image-builder)
for installation instructions.

After you have installed the automotive-image-builder tool, to build images for your R-Car S4 board, you must download
[the `rcar_s4.ipp.yml` configuration file for the Renesas R-Car S4 board](https://gitlab.com/CentOS/automotive/src/automotive-image-builder/-/blob/fb8f97f69d97bbc2eece7bb5d6a51d5f93c997b1/targets/rcar_s4.ipp.yml),
which is available in the `targets` directory of the
[`automotive-image-builder` Gitlab repository](https://gitlab.com/CentOS/automotive/src/automotive-image-builder).

**Prerequisites**

* A Fedora, CentOS, or RHEL system running on an AArch64 architecture
* `gzip`
* A workstation PC configured with a TFTP server

**Procedure**

1. Build the R-Car S4 AutoSD image, replacing `<manifest>` with your custom automotive image builder manifest file in the `.aib.yml` format:

    ```console
    automotive-image-builder --container build --target rcar_s4 --mode package --export image <path>/<manifest> .
    ```

    !!! Note
        If you do not have your own manifest file, you can build
        an AutoSD image by using the example manifest file from `sample-images`. For more information about how to get sample images, see
        [Cloning AutoSD sample images and `automotive-image-builder`](../getting-started/index.md).

1. Compress the image:

    ```console
    gzip cs9-rcar_s4-developer-regular.aarch64.img cs9-rcar_s4-developer-regular.aarch64.img.gz
    ```

1. Place this image onto your TFTP server.

### Flashing the image

To flash the AutoSD image onto your R-Car S4, you must copy the image from your TFTP server onto your board.

**Prerequisites**

* A workstation PC configured with a TFTP server that hosts your AutoSD image
* A network connection between the PC and the R-Car S4 board

**Procedure**

1. Download the image file to `0x480000000`:

    ```console
    $ tftp 0x480000000 cs9-rcar_s4-developer-regular.aarch64.img.gz
    ```

2. Copy the image to the MMC device `0`:

    ```console
    $ gzwrite mmc 0 0x480000000 ${filesize} 1000000 0
    ```

### Configuring U-Boot to boot the image

To boot the AutoSD image, you must configure U-Boot on the R-Car S4 to boot from the image.

**Prerequisites**

* An AutoSD image on MMC device `0`

**Procedure**

1. Set the `bootcmd` environment variable to load AutoSD and start AutoSD from the MMC device:

    ```console
    setenv bootcmd 'mmc dev 0; part start mmc 0 boot_a boot_start; part size mmc 0 boot_a boot_size; mmc read $loadaddr $boot_start $boot_size; abootimg get dtb --index=0 dtb0_start dtb0_size; setenv bootargs androidboot.slot_suffix=_a; bootm $loadaddr $loadaddr $dtb0_start'
    saveenv
    ```

2. To boot into AutoSD immediately, reset the board using the `reset` command.
3. To start the boot process manually, use the `run bootcmd` command.

**Additional Resources**

* [R-Car S4 Startup Guide from the Renesas R-Car S4 support page](https://www.renesas.com/us/en/products/automotive-products/automotive-system-chips-socs/r-car-s4-automotive-system-chip-soc-car-servercommunication-gateway)
