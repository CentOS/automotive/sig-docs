# Continuous certification for safety-critical applications

By extending CentOS Stream to the automotive industry through its automotive variant AutoSD, automakers and automotive safety enthusiasts
alike are able to rapidly innovate on and contribute to the development of an open and functionally safe platform.

!!! IMPORTANT
    Although it incorporates features that support functionally safe automotive Linux, **AutoSD is not ISO 26262 certified**.

    Out-of-context functional safety certification applies only to Red Hat In-Vehicle OS.
    Hardware vendors are responsible for the out-of-context certification of their hardware.
    Automakers are responsible for the in-context ISO 26262 certification of their implementation of the
    in-vehicle OS on their target hardware, with or without support from software and hardware vendors,
    depending on their contractual agreements.

Once AutoSD flows downstream to Red Hat In-Vehicle OS, Red Hat engineering teams perform additional safety verification and validation
specific to the context of each automotive target hardware reference platform before each release. In coordination with our certification partner,
[exida](https://www.redhat.com/en/about/press-releases/red-hat-sets-sights-delivering-first-continuously-certified-linux-platform-vehicles?sc_cid=7013a000002gz3rAAA),
Red Hat is pursuing certification of Red Hat In-Vehicle OS as a safety element out of context (SEooC) up to ASIL B according to ISO 26262 2nd ed.
The safety approach is a tailored, open-source interpretation of ISO 26262 that also considers the informative annex
ISO/PAS 8926:2024.

This tailored approach is an extension to the activities described in ISO 26262:2018-8 section 12 titled, “Qualification of software components" and
expanded in and ISO/PAS 8926:2024, a framework for FuSa that enables the use of preexisting software
architectural elements not originally developed in accordance with ISO 26262:2018. Red Hat In-Vehicle OS is composed of preexisting open source software
that was created following an industry-standard, open source development process that is not compliant with any current safety standards. Therefore,
the activities performed by Red Hat are intended to supplement the rigors of the open source software process, thereby establishing the pedigree and
suitability of the OS for use in an automotive FuSa context. Thus, Red Hat is collaborating with automotive and safety communities to design a novel
continuous functional safety (FuSa) certification approach for Red Hat In-Vehicle OS. Most importantly, because of our efforts to automate many of the
safety certification steps, Red Hat expects recertification to take only a fraction of the existing timeline.

**Additional resources**

* [ISO 26262-6:2018: Road Vehicles, Functional Safety, Product development at the software level](https://www.iso.org/standard/68388.html)
* [SO/PAS 8926:2024: Road Vehicles, Functional Safety, Use of preexisting software architectural elements](https://www.iso.org/standard/83346.html)
