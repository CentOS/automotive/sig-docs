# Source and binary distributions

AutoSD is a binary distribution that consists of precompiled RPM packages that contain components for both the automotive Linux kernel and user
spaces.

Automotive developers are likely most familiar with source distributions, which are well known in the embedded ecosystem. Binary distributions, such
as CentOS Stream or AutoSD, are better known in the enterprise world, which has relied on binary distributions for many years. Learn the distinctions
 between source and binary distributions.

## Source distributions

The most well-known tool to build source distributions in automotive and embedded scenarios is Yocto. Yocto builds OS images from sources by using
instructions called recipes. These recipes describe how to build individual components that constitute the OS and how to assemble the OS image from
these different components.

When you build the image, you can generate a cache to avoid restarting from scratch every time. Yocto also has mechanisms you can use to share these
caches, in a git repository, for example. However, these mechanisms are complex to set up and use.

**Build workflow for source distributions**

![build-workflow-source-dist](../img/build-workflow-source-dist.jpg)

## Binary distributions

In contrast to the build workflow for source distributions, binary distributions introduce a distinct step for you to compile the upstream projects
into a binary package, such as `.rpm`. You then use a different set of instructions to _compose_ or assemble the binary package `.rpm`  files into
an OS image.

**Build workflow for binary distributions**

![build-workflow-binary-dist](../img/build-workflow-binary-dist.jpg)

In RPM-based binary distributions, the build instructions are stored in **spec
files** which provide a number of metadata as well as the instructions
to compile, install, or test a specific software.

In the Automotive SIG, the tool used for the OS assembly is called `automotive-image-builer` and
its input files are `automotive image builder manifests` which describe the specificities to the image
to build (list of packages, filesystem layout, user or group creation, and other configurations).

**Additional resources**

* For more information about `kernel-automotive` work in progress, see
[the Kernel Automotive repo](https://gitlab.com/CentOS/automotive/rpms/kernel-automotive).
