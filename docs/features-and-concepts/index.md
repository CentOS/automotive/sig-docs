# Features and concepts

AutoSD is a binary distribution developed within the SIG that is a public, in-development preview of the upcoming Red Hat In-Vehicle
Operating System (OS). Salient benefits of AutoSD include the following:

* Accelerated innovation in cooperation with Red Hat, a software-defined vehicle (SDV) partner with expertise in open source community
development and experience stabilizing, securing, and certifying open software for the enterprise
* Fast, independent, architecture-specific OS installation and maintenance from a precompiled binary distribution
* Streamlined software installation and management from RPM packages built and maintained by using the RPM Package Manager
* Containerized ASIL and QM application isolation for mixed criticality, freedom from interference (FFI), uniform security, and independent
application and OS lifecycles
* Orchestration for services within and between ASIL and QM environments without compromising their integrity
* An image-based, immutable OS with optimized startup times that supports atomic A/B updates and rollbacks with storage- and bandwidth-friendly
updates
* Reduced latency from a reactive, real-time, deterministic, and preemptive automotive kernel
