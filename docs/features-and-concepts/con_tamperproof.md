# Immutable, atomic, tamperproof OS

AutoSD uses [OSTree](https://ostreedev.github.io/ostree/) to deliver an image-based, immutable OS that supports atomic A/B updates
and rollbacks. This means if the system were to
[crash or lose power during an update, it will either reflect the old system or the new one](https://ostreedev.github.io/ostree/atomic-upgrades/#you-can-turn-off-the-power-anytime-you-want).

As a content-addressed object store with inherent deduplication and byte-level image diff features, OSTree is both storage and
bandwidth-friendly. It also offers the capability to dynamically install containerized applications, which makes it possible to
manage application lifecycles independently from the underlying OS lifecycle. When combined with
[composefs](https://github.com/containers/composefs) verification and [UEFI Secure Boot](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/managing_monitoring_and_updating_the_kernel/signing-a-kernel-and-modules-for-secure-boot_managing-monitoring-and-updating-the-kernel#con_what-is-uefi-secure-boot_signing-a-kernel-and-modules-for-secure-boot)
, AutoSD is tamperproof.

## Tamperproofing with composefs

OSTree already uses a content-addressed object store. However, it normally requires check out into a regular directory by using
hard links into the object store for regular files. This directory then bind-mounts as the `rootfs` when the system boots. While
OSTree already supports enabling `fs-verity` on the files in the store, nothing can protect against changes to the checkout
directories. A malicious user can add, remove, or replace files there. The system must always block access to altered files
before reading, especially for hazardous assignments. Composefs addresses these challenges, guaranteeing the integrity and
authenticity of the filesystem.

* It provides a heightened level of separation and isolation, enabling more stringent control over the distinct underlying
persistent `root` filesystem compared to pure OSTree, where a simple `mount -o remount,rw /usr` allows direct, persistent
modification of the underlying files.
* It “seals” a system so that one can only store code produced or signed by a trusted party.

Instead of checking out to a directory, you generate a `composefs` image that points into the object store and mount that
as the `rootfs`. You then enable `fs-verity` on the `composefs` image and embed that digest in the kernel command line, which
specifies the `rootfs`. Since `composefs` generation is reproducible, you can even verify that the `composefs` image you
generated is correct by comparing its digest to one in the OSTree metadata generated when the OSTree image was built.

## Optimized performance and OTA with OSTree and composefs

Composefs also brings better performance to OSTree-based systems with faster reads and better deduplication.

Containers heavily benefit from `composefs` as data files are shared in the page cache. This allows you to launch multiple
container images that can reliably share memory. Furthermore, for OCI (implemented by Podman), a common approach is to untar
each layer by itself, and then use `overlayfs` to stitch them together at runtime. This approach ensures that identical
layers are shared. However, if instead you store the file content in a content-addressed fashion, then you can either generate
a `composefs` file for each layer while continuing to mount them with a chain of `overlayfs` or generate a single `composefs`
for the final merged file system tree.

This allows sharing of content files between images, even if the metadata (like the timestamps or file ownership) vary between
images. When used together with something like `zstd:chunked`, this will speed up pulling container images and make them available
 for use, without the need to create these files if they are already present. This influences concepts such as mixed criticality,
 orchestration of workloads, and application lifecycle.
