# Content definition in Content Resolver

The Automotive SIG relies on
[Content Resolver](https://tiny.distro.builders/)
to define and select content, using CentOS Stream and CBS repositories
as its dependency-resolution backends.

## Definition layout

Content Resolver operates with four distinct input-definition types:

**Repositories**
: *Repositories* group one or more package repositories and prioritize
them for dependency resolution. The SIG uses CentOS Stream
repositories and [CBS](https://cbs.centos.org/) and
[copr](https://copr.fedorainfracloud.org/groups/g/centos-automotive-sig/coprs/) as fallbacks. With this mechanism, we can
include packages that are not currently part of CentOS Stream.

**Environments**
: *Envionments* represent the minimum recommended installation
footprint. They list top-level, standard packages expected in
every in-vehicle installation. The Automotive environment includes a
set of packages required to boot the system on the target hardware
platform, as well as additional components identified during functional
safety and security requirements reviews.

**Workloads**
: *Workloads* layer additional packages on top of the
essential in-vehicle environment or extend the Automotive repository
with new content that might be useful for development and testing. The
two standard, defined workloads are the in-vehicle and the off-vehicle
sets. The in-vehicle workload is empty and only inherits the environment
component list. The off-vehicle workload extends the environment
component list with various development tools. Additional experimental workloads
can be added to visualize and analyze the impact of including new packages.

**Views**
: *Views* unify the resolved workloads into a single, flat
list of packages. The view effectively represents the Automotive
package repository and can be used to build them in practice.

For more information about these input types, see the
[Content Resolver README](https://github.com/minimization/content-resolver#content-resolver) on GitHub.

## Adding your own content

To add new content to Content Resolver, the Automotive SIG recommends
defining a new workload to extend the default environment. Definitions are managed on
[GitHub](https://github.com/minimization/content-resolver-input).

Use the `automotive` label to inherit the base in-vehicle environment
automatically, and include the newly added package components into the
unified view. You can choose to only list top-level packages because the service
automatically resolves dependencies, but you can also list dependencies explicitly.

If the workload requires content not present in CentOS Stream or CBS, add
additional repositories to the Automotive repository definition. Use a
lower priority for the custom repositories to avoid masking content
that comes from CentOS Stream or CBS.
