# Automotive SIG repositories

Automotive SIG members have several repositories where they can find or contribute to automotive-specific code and RPM packages.
For more information about contributing to these repositories, see [Contributing packages](contributing-packages.md).

## Automotive SIG repositories

The [Automotive SIG git repository](https://gitlab.com/CentOS/automotive) offers additional or divergent packages that are tailored for
automotive use cases and compatible with AutoSD and CentOS Stream.
It has the same structure as the CentOS Stream `rpms` and `src` namespaces.

All SIG members can request packages to be distributed through these repositories, provided they meet
[CentOS requirements for SIG](https://wiki.centos.org/SpecialInterestGroup#Requirements).
The Automotive SIG recommends using different branches with clear names for your work in the Automotive SIG repository.

Automotive SIG community members can use and contribute to projects within Automotive SIG repositories to develop and test
public proofs of concept for CentOS automotive variants, like AutoSD, automotive software infrastructure,
and automotive hardware reference platforms.

Not all innovation attempts succeed. These Automotive SIG packages might be works in progress that eventually land in AutoSD,
or they might be experimental work that remains outside of AutoSD, such as research and development concepts or integration ideas.

## AutoSD repositories

Because AutoSD is the public, in-development version of the Red Hat Automotive product, direct access to the AutoSD repositories
is restricted to Red Hat employees. However, anyone in the community can propose changes the AutoSD repositories.
Change proposals for AutoSD can take many forms, you can submit a change request using one or more of the following methods:

* Submit a pull or merge request against an AutoSD package.
* Post a request on the [centos-automotive-sig](https://lists.centos.org/mailman/listinfo/centos-automotive-sig) mailing list.
* Contact your [Red Hat partner manager](https://connect.redhat.com/) when you open a pull or merge request.

### AutoSD nightly RPM package composes

The [AutoSD nightly repo](https://autosd.sig.centos.org/AutoSD-9/nightly/repos/AutoSD/compose/AutoSD/) stores all AutoSD RPM packages
composed by Red Hat Automotive SIG members.

Red Hat Automotive SIG members use configurations of the distribution compose tool
[Pungi](https://pagure.io/pungi/), stored in the
[AutoSD repository](https://gitlab.com/redhat/edge/ci-cd/pipe-x/autosd) of [PipeX](https://gitlab.com/redhat/edge/ci-cd/pipe-x),
to compose Automotive SIG source code and RPM packages into
the AutoSD builds using the [Koji](https://pagure.io/koji) build system.
[PipeX](https://gitlab.com/redhat/edge/ci-cd/pipe-x/what-is-pipe-x)
is a Red Hat Edge continuous improvement / continuous development (CI/CD) workflow system built upon repositories for tooling
and package-level pipelines-as-code using GitLab CI.

For more information about the
CentOS Build System (CBS) tags you need to contribute packages to AutoSD, see [AutoSD CBS tags](cbs.md#autosd-cbs-tags).

#### Downloading AutoSD nightly RPM package composes

The latest version of AutoSD is available at [https://autosd.sig.centos.org](https://autosd.sig.centos.org).
You do not need to download the entire AutoSD repository unless you intend to
build offline using customized manifests. All sample image manifests
require a remote connection to the main CentOS repositories.

**Prerequisites**

* [`wget`](https://www.gnu.org/software/wget/)
* At least 52 GB of available disk space, which changes frequently as the SIG enhances the `aarch64`, `x86_64`, and `noarch` package set

**Procedure**

1. Optional: Install `wget`:

    ```console
    sudo dnf wget
    ```

1. Download a local copy of the repository using `wget` or your preferred download method:

    ```console
    $ wget --recursive --no-parent -Rc "index.html*" \
    'https://autosd.sig.centos.org/AutoSD-9/nightly/repos/AutoSD/compose/AutoSD/'
    ```

## CentOS Stream git repositories

The [CentOS Stream](https://gitlab.com/redhat/centos-stream/) repository stores the CentOS Stream source code and RPM packages used for
Automotive and AutoSD in the following namespaces:

**[`rpms`](https://gitlab.com/redhat/centos-stream/rpms)**
: The CentOS Stream [`dist-git`](https://sigs.centos.org/guide/git/#the-flat-dist-git-layout) is the dedicated git
repository for each CentOS package. It contains the spec files and patches used to build CentOS Stream RPMs, and it has a
`sources` file that points to a specific file that the build system retrieves from the lookaside-cache.

**[`src`](https://gitlab.com/redhat/centos-stream/src)**
: The CentOS Stream [source-git](https://docs.centos.org/en-US/stream-contrib/contributing/source-git/) is the
source code git repository for CentOS Stream [9](https://gitlab.com/redhat/centos-stream/src/kernel/centos-stream-9) and
[10](https://gitlab.com/redhat/centos-stream/src/kernel/centos-stream-10) where the packages files as well as the untarballed source code
is stored. It has automation that automatically syncs from the `source-git` to the `dist-git`. However, it does not store
every CentOS stream package, only those that have opted into this model.

You can browse Automotive and AutoSD packages available from CentOS Stream in the
[the CentOS Stream mirror](https://mirror.stream.centos.org/SIGs/9-stream/).
For more information, see the [CentOS Stream Quick start guide for new contributors](https://docs.centos.org/en-US/stream-contrib/quickstart/).

## Fedora and EPEL repositories

The [Fedora](https://fedoraproject.org/) community works on a project called
[Extra Packages for Enterprise Linux (EPEL)](https://docs.fedoraproject.org/en-US/epel/). RPM packages that are part of EPEL are compatible
with CentOS Stream, AutoSD, RHEL, and Red Hat In-Vehicle OS.

You can browse available Fedora RPM packages at [Fedora Package Sources](https://src.fedoraproject.org/projects/rpms/*), or
browse Fedora-based builds from the Automotive SIG on the
Fedora build system, [copr](https://copr.fedorainfracloud.org/groups/g/centos-automotive-sig/coprs/).

For examples of how you might use EPEL packages in AutoSD, see [AutoSD non-sample images](../getting-started/sample-non-sample-images.md#autosd-non-sample-images).
