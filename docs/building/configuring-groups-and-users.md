# Configuring groups and users

Configure security and isolation for your image by granting role-based access to the QM and ASIL partitions
or specific directories within those partitions. You can view a complete example manifest for user and group
configuration in the [demos/users/users.aib.yml](https://gitlab.com/CentOS/automotive/sig-docs/-/tree/main/demos/users/users.aib.yml?ref_type=heads) file.

**Prerequisites**

* A custom manifest file, such as the manifest file that you created in
  [Embedding RPM packages in the AutoSD image](packaging_apps_with_rpm.md#embedding-rpm-packages-from-local-storage-into-the-autosd-image).

**Procedure**

To configure groups and users for your OS image, add the following stages to your custom image manifest.

`content`
: Creates user directories.

`groups`
: Creates group accounts with configurable group IDs (gid).

`users`
: Adds or modifies user accounts with configurable user IDs (uid).

1. Optional: Create directories for users if they don't already exist:

    ```console title="automotive image builder manifest"
    --8<-- "demos/users/users.aib.yml:5:9"
    ```

1. Create groups for your users. In this example, create two groups called `guest` and `foo`:

    ```console title="automotive image builder manifest"
    auth
    --8<-- "demos/users/users.aib.yml:30:34"
    ```

1. Create users. In this example, create and assign the `guest` user to the `guest` group, with a preconfigured password
and custom home directory. Create and assign the `foo` user to the `foo` group, and prevent login:

    ```console title="automotive image builder manifest"
    --8<-- "demos/users/users.aib.yml:36:46"
    ```

**Next steps**

* Build your image
* Continue customizing your image

For more information about these stages, see the automotive-image-builder [parameter schema](https://centos.gitlab.io/automotive/src/automotive-image-builder/simple_manifest.html).
