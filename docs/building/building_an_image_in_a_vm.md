# Building an image in a VM

The `makefile` configuration supports running OSbuild inside a VM. There are two scenarios in which this is beneficial:

- Standard OSBuild requires root/sudo permissions, because it performs some system-level operations such as loopback mounts.
  Running OSBuild in a VM allows you to run it non-privileged.
- When you build an image for a different architecture, you can use QEMU software emulation to make this work.
  Software emulation is slower than native, but for some use cases it is sufficient.

Running `make osbuildvm-images` uses OSBuild on the host to build the supporting image files, `_build/osbuildvm-*`.
These files are required to build an image in a VM.
The supporting image files can later be used to build other images from the same architecture inside a VM by passing `VM=1` to `make`.

To build images from a different architecture in a VM, there are two options:

- Copy the output of `make osbuildvm-images` from a build on a different architecture into `_build`.
  For example, the contents of _build/osbuildvm-* from an aarch64 machine into `_build/` on an x86_64 machine.

- Use pre-built osbuildvm images. To build for aarch64 on an x86_64 host, follow the steps outlined below.

  Change to the sample-images directory

  ```console
  cd sample-images/
  ```

  Retrieve the nightly osbuildvm* files and place them into the `_build` folder

  ```console
  wget -r -l 1 -nH -nd -np -A 'osbuildvm*' -P _build https://autosd.sig.centos.org/AutoSD-9/nightly/osbuildvm-images/
  ```

After the `_build` folder is populated with the relevant `osbuildvm-*` files, you may then run `make` with a target that has
an architecture different to the host system, for example, make `cs9-qemu-minimal-ostree.aarch64.qcow2`.

!!! note

    You can download pre-built osbuildvm-images files for the aarch64 architecture from the nightly folder at:
    https://autosd.sig.centos.org/AutoSD-9/nightly/osbuildvm-images/

!!! note

    If you have a previous version of `osbuildvm-images` for a different architecture, rebuild or refresh them by using `VM=1` on your architecture.
