# Sample automotive image builder manifest

Refer to this sample automotive image builder manifest as a complete example of the configuration that you perform as part of
the _Building applications for AutoSD_ workflow:

* One RPM installed in the default ASIL location.
* One RPM installed in the QM partition.
* One ASIL container.
* One QM container.

```console
--8<-- "demos/ipc_between_qm_asil_2/ipc_between_qm_asil_2.aib.yml"
```
