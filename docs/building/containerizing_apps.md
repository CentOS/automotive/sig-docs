# Containerizing applications

An alternative to packaging your software with RPM is to install your application or service in a Linux container. Deploying applications as
containers can be advantageous because the container isolates the application from the OS, as well as from other containers. This means that you can
build the application against a different base OS that is not necessarily compatible with AutoSD, with the exception of the kernel application binary
 interface (ABI). With this framework, you can have multiple applications that use different environments running on a single system.

In addition, containers have other advantages, such as the ability for each container to use different versions of dependencies and the improved
robustness, security, and flexibility that comes from the kernel-level application isolation. This isolation forms the mixed-criticality
architecture of the AutoSD application environment.

**Mixed-criticality workloads**

![The isolation aspects of containers support separated services and align with the requirement for well-defined interfaces between containers.](../img/container-isolation-simple.jpg)

## Building a container image for your software

A container image is a lightweight, standalone software package that includes the code, tools, libraries, and settings required to run a piece of
software. The configuration for a container image is stored in a file called a `Containerfile`.

**Prerequisites**

* Podman
* An RPM package (`auto-apps`) in an RPM package repository (`/var/tmp/my_repo`)

**Procedure**

1. Create a `Containerfile` that includes the RPM package that you created in
[Packaging your application source code with RPM](packaging_apps_with_rpm.md#packaging-your-application-source-code-with-rpm):

    ```text
    FROM quay.io/fedora/fedora:40

    COPY my_repo /tmp/my_repo

    RUN ls -l /tmp && ls -l /tmp/my_repo

    RUN dnf install -y /tmp/my_repo/x86_64/auto-apps-0.1*.rpm && dnf clean all
    ```

1. Copy the RPM repository from `/var/tmp/my_repo` to the same directory where you have created the `Containerfile` file:

    ```console
    $ cp -r /var/tmp/my_repo .
    ```

1. Run `podman build` in the same directory as your `Containerfile` to build the
container image, and name the container image `auto-apps`:

    ```console
    $ sudo podman build -t localhost/auto-apps:latest -f Containerfile
    ```

1. Start a container from your `auto-apps` container image,

    ```console
    $ sudo podman run -it auto-apps
    ```

1. From within the running container, verify that your RPM package is present.

    ```console
    $ rpm -q auto-apps
    ```

    If the auto-apps RPM was embedded successfully in the container, the output of the `rpm -q` command displays the version of your package:

    ```console
    $ auto-apps-0.1-1.fc40.x86_64
    ```

Now that you have a functional `auto-apps` container image, you can embed your
containerized applications in the OS image with OSBuild.

## Embedding containerized applications in an OS image

After creating a container image for your application, you can embed it in an  OS image.

Depending on your needs, you can use one of two methods to embed the container image in your operating system image:

* **[Recommended method](#embedding-container-images-from-a-remote-registry)**: Create the container image and add it to a remote container registry.
Then, pull the container image from the remote
  registry to include in the OS image.
This method is the only reliable, reproducible method for OS image builds and is therefore recommended.
* **[Development method](#embedding-local-containerized-applications)**: Create the container image and add it to a local RPM repository. Then, pull
the container image from your local repository to include in the OS image.
Only use this method to build OS images for development and experimentation purposes.

### Embedding container images from a remote registry

You can download and embed containerized applications from [a remote container image registry](https://www.redhat.com/en/topics/cloud-native-apps/what-is-a-container-registry)
  when you build an AutoSD operating system (OS) image. Doing so makes the containerized application available for use from within the AutoSD OS after
  the system boots.

When you use this method, OSBuild pulls a container from a remote container image registry at build time and embeds it in the AutoSD image.

!!! Important
    If you want to include containerized applications in an OS image, pulling containers from a secure remote registry is the only reliable,
     reproducible, and recommended method for production OS image builds.
    Although you can also containerize applications locally and embed them from local storage when building an OS image, doing so is not the
    method recommended for building the OS image and is meant only for development or experimental purposes.

**Prerequisites**

* You are running a Linux-based host on your local machine.
* You have installed the following software on your local machine:
    * [Podman](https://podman.io/)
    * [Skopeo](https://github.com/containers/skopeo/blob/main/install.md)
    * [ostree](https://ostreedev.github.io/ostree/)
    * [PyYAML](http://PyYAML) for Python 3
* You have network access to [a remote container image registry](https://www.redhat.com/en/topics/cloud-native-apps/what-is-a-container-registry).
* You can access a containerized application image on the remote registry. This image can be the `auto-apps` image you built according to
[Building a container image for your software](#building-a-container-image-for-your-software) and then added to the registry.
  You can also use any other containerized application images available in the registry.
* You have installed the `automotive-image-builder` tool. For more information, see
[Installing Automotive image builder](../getting-started/index.md#installing-automotive-image-builder).

**Sample procedure**

This sample procedure explains how to create an automotive image builder manifest and build an AutoSD operating system image in the `qcow2` format
that you can boot in a QEMU virtual machine.

The resulting AutoSD OS image will include a containerized CentOS 10 application image pulled from the [quay.io remote container image registry](http://quay.io)
and installed to `/usr/share/containers/storage` in the AutoSD OS.

Note that you can customize the script and the manifest to pull your own containerized application images from any image registry to
which you have access.

1. Create a build manifest file named `container_remote.aib.yml`:

     ```console
     $ touch container_remote.aib.yml
     ```

1. Open the `container_remote.aib.yml` file in a text editor, add the following YAML code, and save the file:

    ```yaml title="Sample manifest for embedding remote container images"
    --8<-- "demos/container_remote/container_remote.aib.yml:1:34"
    ```

1. Export the value of your hardware architecture to a temporary variable:

    ```console
    $ export arch=$(arch)
    ```

1. Run the `automotive-image-builder` tool to build an OS image that embeds the remote containerized application:

    ```console
    $ sudo automotive-image-builder \
        --verbose \
        --include=/var/lib/containers/storage/ \
        build \
        --distro autosd \
        --target qemu \
        --mode image \
        --build-dir=_build \
        --export qcow2 \
        container_remote.aib.yml \
        container_remote.$arch.qcow2
    ```

    !!! Note
         You can customize the `automotive-image-builder` command to [set other options](../getting-started/about-automotive-image-builder.md#understanding-the-automotive-image-builder-options)
         for `distro` name, `mode`, `target`, and `export` format.

1. Verify that the script has created an AutoSD image file named `container_remote.<arch>.qcow2` in your present working directory.

After you have created the OS image, assuming that you have
[installed QEMU](https://www.qemu.org/download/#linux), you can boot
the AutoSD image in a virtual machine by using the [`automotive-image-runner` utility](../building/building_an_os_image.md#building-and-running-your-customized-os-image)
and verify that the containerized application is included in the built OS image:

1. Run the image in QEMU using `automotive-image-runner`:

    ```console
    $ sudo automotive-image-runner container_remote.x86_64.qcow2
    ```

    If necessary, substitute the filename of your `.qcow2` image file.

1. After the image has booted in QEMU, log in with the user name `root` and the password `password`.

1. Verify that your containerized application image is available:

    ```console
    # podman image list
    ```

    Sample output:

    ```text
    REPOSITORY           TAG         IMAGE ID      CREATED            SIZE
    localhost/cs10  latest      054a7b912609  About an hour ago  250 MB
    ```

When you build your OS image, OSBuild copies the application container image from the remote container image
registry to your OS image.
As defined in the build manifest file, the containerized application is available at `localhost/cs10` and can be run by using Podman.
However, you must first create container configuration files and configure OSBuild to copy these files to the
    `/etc/containers/systemd` directory in your OS image. For more information about these container configuration files, see
    [Running containers from `systemd`](#running-containers-from-systemd).

**Next steps**

* If required, you can customize your image further. For more information, see [Running containers from `systemd`](#running-containers-from-systemd),
[Configuring communication between ASIL containers](configuring_communication_asil_containers.md#configuring-communication-between-asil-containers),
and [Deploying applications in the QM partition](deploying_apps_in_qm_partition.md#deploying-applications-in-the-qm-partition).

**Additional resources**

* [Sample automotive image builder manifest](ref_sample-yaml.md)

### Embedding local containerized applications

You can embed containerized applications from local storage when you build an AutoSD operating system (OS) image. Doing so makes the containerized
application available for use from within the AutoSD OS after the system boots.
This method uses the `containers-storage` option in the build manifest so that OSBuild pulls containers from local container image storage at build
time and includes them into the OS image.

!!! Important
    Although you can containerize applications locally and embed them when building an OS image, doing so is not the recommended method of building
    the OS image and is meant only for development or experimental purposes. If you want to include containerized applications in an OS image, pulling
    containers from a secure remote container image registry is the only reliable, reproducible, and recommended method for OS image builds.

**Prerequisites**

* You are running a Linux-based host on your local machine.
* You have installed the following software on your local machine:
    * [Podman](https://podman.io/)
    * [Skopeo](https://github.com/containers/skopeo/blob/main/install.md)
    * [ostree](https://ostreedev.github.io/ostree/)
    * [PyYAML](http://PyYAML) for Python 3
* You have created a container image named `auto-apps` that contains your application built according to [Building a container image for your software](#building-a-container-image-for-your-software)
* You have installed the `automotive-image-builder` tool. For more information, see
[Installing Automotive image builder](../getting-started/index.md#installing-automotive-image-builder).

**Sample procedure**

This sample procedure explains how to create an automotive image builder manifest and build an AutoSD operating system image in the `qcow2` format
that you can boot in a QEMU virtual machine.

The resulting AutoSD OS image will include a containerized auto-apps application image pulled from local storage
and installed to the `/usr/share/containers/storage` directory in the OS.

Note that you can customize the script and the manifest to pull your own containerized application images from local storage.

1. Create a build manifest file named `container_local.aib.yml`:

     ```console
     $ touch container_local.aib.yml
     ```

1. Open the `container_local.aib.yml` file in a text editor, add the following YAML code, and save the file:

    ```yaml title="Sample manifest for embedding local container images"
    --8<-- "demos/container_local/container_local.aib.yml:1:35"
    ```

1. Export the value of your hardware architecture to a temporary variable:

    ```console
    $ export arch=$(arch)
    ```

1. Run the `automotive-image-builder` script to build an OS image that embeds the remote containerized application:

    ```console
    $ sudo automotive-image-builder \
        --verbose \
        --include=/var/lib/containers/storage/ \
        build \
        --distro autosd9 \
        --target qemu \
        --mode image \
        --build-dir=_build \
        --export qcow2 \
        container_local.aib.yml \
        container_local.$arch.qcow2
    ```

1. Verify that the script has created an AutoSD image file named `container_local.<arch>.qcow2` in your present working directory.

After you have created the OS image, assuming that you have
[installed QEMU](https://www.qemu.org/download/#linux), you can boot
the AutoSD image in a virtual machine by using the [`automotive-image-runner` utility](../building/building_an_os_image.md#building-and-running-your-customized-os-image)
and verify that the containerized application is included in the built OS image:

1. Run the image in QEMU using `automotive-image-runner`:

    ```console
    $ sudo automotive-image-runner container_local.x86_64.qcow2
    ```

    If necessary, substitute the filename of your `.qcow2` image file.

1. After the image has booted in QEMU, log in with the user name `root` and the password `password`.

1. Verify that your containerized application image is available:

    ```console
    # podman image list
    ```

    Sample output:

    ```console
    REPOSITORY           TAG         IMAGE ID      CREATED            SIZE
    localhost/auto-apps  latest      054a7b912609  About an hour ago  250 MB
    ```

When you build your OS image, OSBuild copies the `auto-apps` container image to your OS image. Your containerized `auto-apps` application is
available at `localhost/auto-apps`. You must also create container configuration files and configure OSBuild to copy these files to the
`/etc/containers/systemd` directory in your OS image. For more information about these container configuration files, see
[Running containers from `systemd`](#running-containers-from-systemd).

**Next steps**

* Now that you have included your containerized application in your automotive image builder manifest, you can build your AutoSD image.
  For more information, see [Building an AutoSD image](building_an_os_image.md#building-an-autosd-image).
* Alternatively, you can continue customizing your image. For more information, see [Running containers from `systemd`](#running-containers-from-systemd),
  [Configuring communication between ASIL containers](configuring_communication_asil_containers.md#configuring-communication-between-asil-containers),
 and [Deploying applications in the QM partition](deploying_apps_in_qm_partition.md#deploying-applications-in-the-qm-partition).

**Additional resources**

* [Sample automotive image builder manifest](ref_sample-yaml.md)

## Running containers from `systemd`

When you embed a container in an operating system (OS) image, you can start the container manually in the booted system with the `podman run`
command. However, the container does not start automatically at boot time. To configure a container to start at boot time, you must create a
`systemd` service that starts the container at the right time, in the right way.

Quadlet is a tool that optimally runs Podman containers under `systemd`. Rather than creating the `systemd` service manually, use Quadlet to
automatically generate the corresponding `systemd` service unit file at boot time. In this example, create Quadlet files for the sample applications
that are available in the [AutoSD sample apps](https://gitlab.com/CentOS/automotive/sample-images) repository. If you want to use your own
containerized software, see the [Podman documentation](https://docs.podman.io/en/latest/markdown/podman-systemd.unit.5.html) for more information
about creating your own Quadlet configuration files.

**Prerequisites**

* A container image available inside your OS image at `localhost/auto-apps` embedded according to
[Embedding containerized applications in the AutoSD image](containerizing_apps.md#embedding-local-containerized-applications)
* A custom manifest file, such as the manifest file you created in
[Embedding RPM packages in the AutoSD image](packaging_apps_with_rpm.md#embedding-rpm-packages-from-local-storage-into-the-autosd-image)
* You have installed the `automotive-image-builder` tool. For more information, see
[Installing Automotive image builder](../getting-started/index.md#installing-automotive-image-builder).

**Procedure**

1. Create Quadlet unit files for the `radio-service` and `engine-service` services in your sample application `auto-apps`:

    ```console title="radio.container file"
    --8<-- "demos/radio.container"
    ```

    ```console title="engine.container file"
    --8<-- "demos/engine.container"
    ```

1. Create an automotive image builder manifest named `quadlet_radio_engine.aib.yml` that contains the following code, which copies the Quadlet
   unit files to the `/etc/containers/systemd/` directory during the OS image build process:

    ```yaml title="Manifest configuration to copy Quadlet unit files"
    --8<-- "demos/quadlet_radio_engine/quadlet_radio_engine.aib.yml"
    ```

    !!! note
        The `path:` option resolves a relative path. In this example, your
        Quadlet unit files are in the `../` directory.

1. Run the `automotive-image-builder` tool to build an OS image:

    ```console
    $ sudo automotive-image-builder \
        --verbose --container \
        --include=/var/lib/containers/storage/ \
        build \
        --distro autosd9 \
        --target qemu \
        --mode image \
        --build-dir=_build \
        --export qcow2 \
        quadlet_radio_engine.aib.yml \
        quadlet_radio_engine.$arch.qcow2
    ```

1. Verify that the script has created an AutoSD image file named `quadlet_radio_engine.<arch>.qcow2` in your present working directory.

After you have created the OS image, assuming that you have
[installed QEMU](https://www.qemu.org/download/#linux), you can boot
the AutoSD image in a virtual machine by using the [`automotive-image-runner` utility](../building/building_an_os_image.md#building-and-running-your-customized-os-image):

1. Run the image in QEMU using the `automotive-image-runner` script:

    ```console
    $ sudo automotive-image-runner quadlet_radio_engine.x86_64.qcow2
    ```

    If necessary, substitute the filename of your `.qcow2` image file.

1. After the image has booted in QEMU, log in with the user name `root` and the password `password`.

   **Additional resources**

* [Sample automotive image builder manifest](ref_sample-yaml.md)
* [Make systemd better for Podman with Quadlet](https://www.redhat.com/sysadmin/quadlet-podman)
* [Deploying a multi-container application using Podman and Quadlet](https://www.redhat.com/sysadmin/multi-container-application-podman-quadlet)
