# Building images

Both AutoSD and its downstream RHIVOS are sets of RPM binaries.
You customize and build your own image, including your own applications as RPMs or container images.

[OSBuild](https://www.osbuild.org/) is the tool to build those images
on CentOS Stream, Fedora, or RHEL hosts, with the option to build immutable images using
[OSTree](https://ostreedev.github.io/ostree/introduction/).

## Prerequisites

Before you build images, be aware of the following constraints:

- **No cross-compilation**: To build AArch64 or x86_64 images, you must run OSBuild on the respective systems.
  Some options for AArch64 hosting include Raspberry Pi 4 or QEMU on Linux or macOS.
  For more information, see [Getting started](../getting-started/index.md) on Linux or macOS.

- **A subscribed RHEL system**: To build RHEL images, you must have access to a subscribed RHEL system to access to the entitlements.
  RHEL images can only be built on subscribed RHEL systems.
