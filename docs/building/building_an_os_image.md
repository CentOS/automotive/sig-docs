# Building an AutoSD image

[Automotive Image Builder](https://gitlab.com/CentOS/automotive/src/automotive-image-builder) is a tool used to create Operating System images leveraging
the capabilities of [OSBuild](https://www.osbuild.org/) underneath.

The Automotive Image Builder input files are called manifests, they are YAML files which define the content and configuration
of the desired OS image, such as which repositories and RPM packages to include, which services to enable,
and hostname and time zone configurations.

In this workflow, you have configured your own custom manifest file that you will use to build your own custom AutoSD image.
For further reference, you can also review examples manifests available in:

- The [sig-docs/demos](https://gitlab.com/CentOS/automotive/sig-docs/-/tree/main/demos?ref_type=heads) which
  includes all the manifests present in this documentation site. The images are built and tested nightly and
  upon changes to Automotive Image Builder itself.

- The [sample-images repos](https://gitlab.com/CentOS/automotive/sample-images) which includes a collection
  of manifests. Note that those images are not regularly tested and while most of them are included in
  sig-docs/demos there may still be a few additional interesting manifests examples.

## Building and running your customized OS image

This process is simplified with the help of the `automotive-image-builder` tool, which invokes the OSbuild to build the images
using manifest files that you specify.

**Prerequisites**

- Enable and install Automotive Image Builder, see [Installing automotive-image-builder
  ](../getting-started/about-automotive-image-builder.md#installing-automotive-image-builder)
  for more information.

- An Automotive Image Builder manifest files, such as the manifest file that you created in
  [Embedding RPM packages in the AutoSD image](packaging_apps_with_rpm.md#embedding-rpm-packages-from-local-storage-into-the-autosd-image)
  and extended in the other sections in this guide

**Procedure**

1. Build the AutoSD image. In this example, specify your manifest file and the `qcow2` image format in the build command:

     ```console
     $ automotive-image-builder build --export qcow2 <my-manifest>.aib.yml <my-image>.qcow2
     ```

    The example command demonstrates how to build a QCOW2 image for a virtual environment using the most basic options.
    Additional [options](../getting-started/about-automotive-image-builder.md#understanding-the-automotive-image-builder-options) are available.
    You can also [build images for specific hardware types](../provisioning/index.md).

1. Run the image:

     ```console
     $ automotive-image-runner <my-image>.qcow2
     ```

1. After the image boots, log in as `root` using the password `password`.

1. Verify that your packaged software is present in your image:

     ```console
     # rpm -q auto-apps dlt-daemon vsomeip3-routingmanager
     ```

Your image is now ready.

**Next steps**

- Now that you have built your AutoSD image, you can flash it onto an SD card. For more information, see
  [Flashing the image on an SD card](building_an_os_image.md#flashing-the-image-on-an-sd-card).

**Additional resources**

- [Sample automotive image builder manifest](ref_sample-yaml.md)

## Flashing the image on an SD card

To install the OS on a hardware device, flash the image onto an SD card.

**Prerequisites**

- A built AutoSD image such as the one you created in
  [Building and running your customized OS image](building_an_os_image.md#building-and-running-your-customized-os-image)

**Procedure**

- Flash the image onto an SD card:

```console
$ dd if=<my-image>.qcow2 of=<SD card device path> status=progress bs=4M
```

!!! important

    You must change the value for `of=<SD card device path>` to match the block device path used in your system, for example `/dev/sdb`. If
    required, add `-bios /usr/share/OVMF/OVMF_CODE.fd` to QEMU command.
